﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AndrewBlog.Data.Models;
using Microsoft.AspNetCore.Http;

namespace AndrewBlog.Models.BlogViewModels
{
    public class CreateVIewModel
    { 
        [Required,Display(Name ="Header Image")]
        public IFormFile BlogHeaderImage { get; set; }
        public Blog Blog { get; set; }
    }
}

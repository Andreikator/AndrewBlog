﻿using AndrewBlog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AndrewBlog.Models.AdminViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<Blog> blogs { get; set; }
    }
}
